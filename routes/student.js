const express = require("express");
const router = express.Router();

const {
  hello,
  allStudent,
  findById,
  findByIdandName,
} = require("../controller/student");

router.get("/", hello);
router.get("/student-all", allStudent);
router.get("/student/byid/:id", findById);
router.get("/student/byid/:id([0-9]{1})/byname/:name", findByIdandName);

module.exports = router;