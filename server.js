const express = require("express");
const app = express();

const studentRoutes = require('./routes/student.js');

const productRouts = require("./routes/product.js");

//===========================
//======== Products==========

//GET
//produts
//get all products data
app.use(studentRoutes);
app.use(productRouts)



app.get("*", (req, res) => {
  res.status(400).send("API not found");
});

const port = 4000;
app.listen(port, () => console.log("server is listening on port", port));