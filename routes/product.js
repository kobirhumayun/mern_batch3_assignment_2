const express = require('express')
const router = express.Router()
const {
    products,
    productsQuery
} = require('../controller/product')

router.get("/products", products);

//
router.get("/products/:brand", productsQuery);

module.exports = router