const studentData = require('../data/studentData')

const hello = (req, res) => {
  res.send("Hello Next topper");
};

const allStudent = (req, res) => {
  // route, controller
  if (studentData.length) res.status(200).send(studentData);
  else {
    res.status(400).send({ msg: "No student found" });
  }
};

const findById = (req, res) => {
  const { id } = req.params;
  let student = studentData.filter((ele) => ele.id === parseInt(id));
  res.status(200).send(student[0]);
};


const findByIdandName = (req, res) => {
  const { id, name } = req.params;
  let student = studentData.filter(
    (ele) =>
      ele.id === parseInt(id) && ele.name.toLowerCase() === name.toLowerCase()
  );
  if (student.length) res.status(200).send(student[0]);
  else res.status(400).send({ msg: "No student found" });
};

module.exports = {
    hello,
    allStudent,
    findByIdandName,
    findById
}