const products = [{
    id: 1,
    name: "mobile-cover",
    brand: "Oneplus",
    price: 100,
    color: "blue",
  },
  {
    id: 2,
    name: "mobile-cover",
    brand: "Samsung",
    price: 500,
    color: "blue",
  },
  {
    id: 3,
    name: "mobile-cover",
    brand: "Oneplus",
    price: 400,
    color: "golden",
  },
  {
    id: 4,
    name: "mobile-cover",
    brand: "Samsung",
    price: 2000,
    color: "golden",
  },
  {
    id: 5,
    name: "mobile-cover",
    brand: "Oneplus",
    price: 900,
    color: "black",
  },
  {
    id: 6,
    name: "mobile-cover",
    brand: "Samsung",
    price: 900,
    color: "blue",
  },
];

module.exports = products;