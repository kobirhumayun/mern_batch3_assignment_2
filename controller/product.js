const productData = require('../data/productData')

const products = (req, res) => {
    if (productData.length) res.status(200).send(productData);
    else {
        res.status(404).send({
            msg: "No product found"
        });
    }
}
const productsQuery = (req, res) => {
    const {
        brand
    } = req.params;
    const {
        color,
        price
    } = req.query;
    let priceInfo;

    // if price available then split it
    if (req.query.price) {
        priceInfo = price.split("-").map((ele) => parseInt(ele));
    }

    let queryData = [];

    let filteredData = productData.filter(
        (ele) => ele.brand.toLowerCase() === brand
    );
    console.log(color, price)

    if (color && price) {
        console.log('color = ', color, 'price =', price);
        queryData = filteredData.filter(
            (ele) =>
            ele.color.toLowerCase() === color &&
            ele.price >= priceInfo[0] &&
            ele.price <= priceInfo[1]
        );

    } else if (color) {
        queryData = filteredData.filter((ele) => ele.color.toLowerCase() === color);
    }
    if (queryData.length === 0 && color && price) {
        queryData.push("Color or price not match")
    }


    if (queryData.length) res.status(200).send(queryData);
    else if (filteredData.length) res.status(200).send(filteredData);
    else {
        res.status(404).send({
            msg: "No product found"
        });
    }
}

module.exports = {
    products,
    productsQuery
}